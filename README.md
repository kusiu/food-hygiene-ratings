Food Hygiene Ratings
============

Food Hygien Ratings project created as a technical test for Infinity Works.
Created in Symfony framework 3.2

## Requirements

- PHP 5.5.9 or higher;
- GIT
- composer

## Installation

```bash
$ git clone git@bitbucket.org:kusiu/food-hygiene-ratings.git
$ cd infinity.dev
$ composer install
```
