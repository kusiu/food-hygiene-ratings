<?php

namespace AppBundle\Controller;

use AppBundle\Form\LocalAuthorityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use \GuzzleHttp\Client;
use AppBundle\Form\AuthorityType;

class DefaultController extends Controller
{
    /** @var string Api base Url */
    private $apiBaseUrl = 'http://api.ratings.food.gov.uk';

    /** @var array Api headers */
    private $apiHeaders = [
        "x-api-version" => 2,
        "Accept-Language" => "cy-GB"
    ];

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $form = $this->localAuthorityForm();

        return $this->render('default/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/establishment", name="establishment")
     */
    public function establishmentAction(Request $request)
    {
        $form = $this->localAuthorityForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            // Get list of establishments by provided local authority
            $client = new Client();
            $res = $client->request('GET', $this->apiBaseUrl . '/Establishments?localAuthorityId=' . $data['authority'] . '&pageSize=0', [
                'headers' => $this->apiHeaders
            ]);

            $establishments = json_decode($res->getBody());

            // empty rating list
            $rating = [
                '5star' => 0,
                '4star' => 0,
                '3star' => 0,
                '2star' => 0,
                '1star' => 0,
                'Exempt' => 0,
            ];

            // populate rating list
            foreach ($establishments->establishments as $establishment) {
                if ($establishment->RatingValue == 5) {
                    $rating['5star']++;
                } else if ($establishment->RatingValue == 4) {
                    $rating['4star']++;
                } else if ($establishment->RatingValue == 3) {
                    $rating['3star']++;
                } else if ($establishment->RatingValue == 2) {
                    $rating['2star']++;
                } else if ($establishment->RatingValue == 1) {
                    $rating['1star']++;
                } else {
                    $rating['Exempt']++;
                }
            }

            // convert the values into percentage
            $total = array_sum($rating);
            foreach($rating as &$hits) {
                $hits = round($hits / $total * 100, 1) . '%';
            }
        }

        return $this->render('default/establishment.html.twig', [
            'form' => $form->createView(),
            'rating' => $rating
        ]);
    }

    /**
     * Returns local authority Form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function localAuthorityForm()
    {
        // Get list of local authorities
        $client = new Client();
        $res = $client->request('GET', $this->apiBaseUrl . '/Authorities/basic', [
            'headers' => $this->apiHeaders
        ]);
        $authorities = json_decode($res->getBody())->authorities;

        $authoritiesList = [];
        foreach ($authorities as $authority) {
            if ($authority->SchemeType == 1) {
                $authoritiesList[$authority->Name] = $authority->LocalAuthorityId;
            }
        }

        return $this->createForm(LocalAuthorityType::class, null, array(
            'action' => $this->generateUrl('establishment'),
            'method' => 'POST',
            'authoritiesList' => $authoritiesList,
        ));
    }
}
