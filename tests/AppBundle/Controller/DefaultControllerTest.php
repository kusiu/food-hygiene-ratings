<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use \GuzzleHttp\Client;

class DefaultControllerTest extends WebTestCase
{

    /** @var string Api base Url */
    private $apiBaseUrl = 'http://api.ratings.food.gov.uk';

    /** @var array Api headers */
    private $apiHeaders = [
        "x-api-version" => 2,
        "Accept-Language" => "cy-GB"
    ];
    private $client;

    public function setUp()
    {
        $this->client = new Client();
    }

    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $form = $crawler->selectButton('Show')->form();
        $form['local_authority[authority]'] = 26;
        $crawler = $client->submit($form);

        $this->assertEquals(1, $crawler->filter('.rating:contains("5star")')->count());

    }

    public function testAPILocalAuthorities()
    {
        $res = $this->client->request('GET', $this->apiBaseUrl . '/Authorities/basic', [
            'headers' => $this->apiHeaders
        ]);
        $authorities = json_decode($res->getBody())->authorities;
        $this->assertTrue(count($authorities) > 1);
    }

    public function testAPIEstablishments()
    {
        // Check Watford
        $res = $this->client->request('GET', $this->apiBaseUrl . '/Establishments?localAuthorityId=26&pageSize=0', [
            'headers' => $this->apiHeaders
        ]);

        $establishments = json_decode($res->getBody())->establishments;
        $this->assertTrue(count($establishments) > 1);
    }
}
